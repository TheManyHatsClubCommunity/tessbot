import random

import discord
import os
import tessbot
import apikeys
from models import Service, Server, Chat, User, Session, get_or_create
import config
import logging

client = discord.Client()
error_logger = logging.getLogger("main")
MAX_RETRY=3

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')
    global tess
    session = Session()
    session.expire_on_commit = False
    service = get_or_create(session, Service, name="discord")
    user = get_or_create(session, User, id=client.user.id, service_id=service.id, username=client.user.display_name)

    try:
        session.commit()
    except:
        error_logger.exception("Couldn't commit to database!")
        session.rollback()
    finally:
        session.close()

    tess = tessbot.TessBot(user)

@client.event
async def on_message(message):

    # runs only on bottest channels if debug is enabled.
    if config.DEBUG==True:
        if message.channel.name != "bottest":
            return
    
    # other bots are unworthy of our attention
    if message.author.bot == True:
        return

    if(tess):
        async def sendReply(text, edit=None, append=None):
            async def attempt(count=0):
                if(count < MAX_RETRY):
                    try:
                        if(edit):
                            return await client.edit_message(edit,text)
                        elif(append):
                            return await client.edit_message(append,append.content+text)

                        return await client.send_message(message.channel, text)
                    except Exception as e:
                        error_logger.exception("Couldn't send message")
                        return await attempt(count + 1)
                return None

            if text != "":
                return await attempt()
            return None

        try:
            session = Session()
            session.expire_on_commit = False

            service = get_or_create(session, Service, name="discord")

            if(message.server):
                current_server = get_or_create(session, Server,id=message.server.id, service_id=service.id)
                current_server.server_name = message.server.name
            else:
                current_server = None

            current_user = get_or_create(session, User, id=message.author.id, service_id=service.id)
            current_user.username = message.author.display_name

            if(message.channel.name):
                current_channel = get_or_create(session, Chat, id=message.channel.id, server_id=current_server.id, chat_name=message.channel.name)
                current_channel.nsfw = False
            else:
                current_channel = get_or_create(session, Chat, id=message.channel.id, server_id=current_server.id, chat_name=message.channel.id)
        except:
            error_logger.exception("Couldn't get data from message!")

        metadata = {"session":session, "service": service, "user":current_user, "server":current_server, "chat":current_channel}

        try:
            await tess.read(message.content, metadata, sendReply)
        except Exception as e:
            error_logger.exception("Exception reading message")

        try:
            session.commit()
        except:
            error_logger.exception("Couldn't commit to database!")
            session.rollback()
        finally:
            session.close()

if (__name__ == "__main__"):
    tess = None
    client.run(apikeys.discordkey)
