import os, sys

DEBUG = False
ROOT_DIR = os.path.dirname(sys.modules['__main__'].__file__)

RESPONSE_DIR = os.path.join(ROOT_DIR, "responses")
COMMAND_DIR = os.path.join(ROOT_DIR, "commands")
