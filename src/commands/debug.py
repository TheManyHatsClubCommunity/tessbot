from helpers import commandHelpers
import commandRegistry
import asyncio

commands = {}
command = commandRegistry.command
help_text = commandRegistry.help_text

@help_text("Run debug command")
@command("sh")
async def run(command, metadata, sendReply):
    message = commandHelpers.get_arg_string(command[1])

    if("whoami" in message):
        await sendReply("root")
    elif("cat /etc/passwd" in message):
        await sendReply("root:x:0:0:root:/root:/bin/bash\n\
                        bin:x:1:1:bin:/bin:/usr/bin/nologin\n\
                        daemon:x:2:2:daemon:/:/usr/bin/nologin\n\
                        mail:x:8:12:mail:/var/spool/mail:/usr/bin/nologin\n\
                        ftp:x:14:11:ftp:/srv/ftp:/usr/bin/nologin\n\
                        http:x:33:33:http:/srv/http:/usr/bin/nologin\n\
                        uuidd:x:68:68:uuidd:/:/usr/bin/nologin\n\
                        dbus:x:81:81:dbus:/:/usr/bin/nologin\n\
                        nobody:x:99:99:nobody:/:/usr/bin/nologin\n\
                        tess:x:1000:10::/home/tess:/bin/zsh")
    elif("id" in message):
        await sendReply("uid=0(root) gid=10(wheel) groups=10(wheel)")
    elif("ls" in message):
        await sendReply(". .. .git .gitignore tessbot secret.txt") 
    elif("cat secret.txt" in message):
        await sendReply("Oh please. Surely you didn't think I would actually build a shell into an infosec trivia bot. :^)")
