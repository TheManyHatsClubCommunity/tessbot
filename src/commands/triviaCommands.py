from helpers import basicResponseHelpers, commandHelpers
import config
import commandRegistry
import os
import re
import asyncio
from models import Service, Server, Chat, User, TriviaQuestion, TriviaAnswer, TriviaScore, ActiveQuestion, get_or_create 
from sqlalchemy import func

commands = {}
command = commandRegistry.command
reaction = commandRegistry.reaction
help_text = commandRegistry.help_text

@help_text("Ask a new trivia question")
@command("trivia", "question")
async def trivia(command, metadata, sendReply):
    session = metadata["session"]
    active_question = session.query(ActiveQuestion).filter_by(chat_id=metadata["chat"].id).first()
    new_question = session.query(TriviaQuestion).order_by(func.random()).first()
    
    if(new_question == None):
        return await sendReply("No questions in database!")

    if(active_question == None):
        active_question = ActiveQuestion(chat_id = metadata["chat"].id, question_id = new_question.id)
    else:
        active_question.question_id = new_question.id

    session.add(active_question)

    # Send a trivia question
    await sendReply(restore_linebreaks(new_question.question))


@help_text("Answer the current trivia question")
@reaction("")
@command("answer")
async def answer(command, metadata, sendReply):
    session = metadata["session"]

    active_question = session.query(ActiveQuestion).filter_by(chat_id=metadata["chat"].id).first()

    if(active_question == None and command[0] == "answer"):
        await sendReply("There is no active trivia question! How about this...")
        return await trivia(command, metadata, sendReply)
    
    if(active_question == None):
        return

    answers = session.query(TriviaAnswer).filter_by(question_id=active_question.question_id)
    correct = False
    
    # Answers could be regex also?
    for answer in answers:
        if(answer.case_sensitive == True):
            if(re.match(answer.answer,commandHelpers.get_arg_string(command[1]))):
                correct = True
        else:
            if(re.match(answer.answer, commandHelpers.get_arg_string(command[1]), re.IGNORECASE)):
                correct = True

    if(correct):
        await sendReply(await basicResponseHelpers.randomResponseCommand(command, metadata, os.path.join(config.RESPONSE_DIR, "correct_answer.txt")))
        session.query(ActiveQuestion).filter_by(chat_id=active_question.chat_id).delete()
    else:
        if(command[0] == "answer"):
            await sendReply(await basicResponseHelpers.randomResponseCommand(command, metadata, os.path.join(config.RESPONSE_DIR, "wrong_answer.txt")))

# Command for suggesting new trivia questions
@help_text("Suggest new questions!")
@command("suggest")
async def suggest(command, metadata, sendReply):
    with open("suggested-questions.txt", 'a') as f:
        f.write(commandHelpers.get_arg_string(command[1]))
    await sendReply("Thanks for your suggestion!")


def restore_linebreaks(text):
    return text.replace("\\n", "\n")
