from models import Service, Server, Chat, User, TriviaQuestion, TriviaAnswer, TriviaScore, ActiveQuestion, Session, get_or_create 
import sys
import os

def addFromFile(file):

    session = Session()
    session.expire_on_commit = False

    with open(file) as f:

        for line in f:
            data = line.split("{end}")
            question = data[0]
            answer = data[1]
            print("Question: " + str(question).replace("\\n", "\n"))
            print("Answer: " + str(answer).replace("\\n", "\n"))
            valid = False
            while(not valid):
                print("Ok? (Y/n)")
                resp = input()
                if(resp == "" or resp == "y" or resp == "Y"):
                    valid = True
                elif(resp == "no" or resp == "n" or resp == "N"):
                    return
            trivia_question = TriviaQuestion(question=question.strip(), difficulty=0)
            session.add(trivia_question)
            session.flush()

            trivia_answer = TriviaAnswer(answer=answer.strip(), question_id=trivia_question.id)
            session.add(trivia_answer)

    
        try:
            session.commit()
        except Exception as e:
            print(e)
            session.rollback()



if __name__ == "__main__":
    session = Session()

    if(len(sys.argv) == 3 and sys.argv[1] == "-f"):
        addFromFile(sys.argv[2])
    else:

        while(True):
            print("Enter a question, or q to exit:")
            question = input()

            if(question == "q"):
                break

            valid=False
            while(not valid):
                print("Enter the difficulty of this question out of 10:")
                difficulty = int(input())
                if(difficulty > 0 and difficulty <= 10):
                    valid = True
                else:
                    print("Please enter a difficulty between 1 and 10.")

            trivia_question = TriviaQuestion(question=question, difficulty=difficulty)
            
            session.add(trivia_question)
            session.flush()

            print("Enter the answer:")
            answer = input()
            trivia_answer = TriviaAnswer(answer=answer, question_id=trivia_question.id)

            session.add(trivia_answer)

            try:
                session.commit()
            except Exception as e:
                print(e)
                session.rollback()

